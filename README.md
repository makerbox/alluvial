Sound Trigger Code
===================

For electronic paintings done by artist Jess Weichler; this daemon (internally known as "Alluvial") drives the Raspberry Pi, which currently receives signals from a Makey Makey, so that sounds are triggered when a circuit is completed. 

Eventually, this may be done with GPIO instead, but I can't do that til I get female cables for the GPIO pins of the Pi.

Also included is a Systemd service to automatically start the process upon boot, and a patch for the standard getty@.service to allow automatic login, since the Pi is running as an appliance. This has the Pi acting a lot more like, say, an Arduino, than a Pi; you plug it in, and everything starts without any interaction from you. You don't need a monitor or keyboard, everything just starts automagically.

The Raspberry Pi (which I power with Fedora Linux [also called "Pidora"]), for maximum efficiency, should have lots of components stripped from it; for instance, there's no need for Xorg, network, vim, and so on. It speeds up boot significantly.

This code is probably not useful to you unless you work at Makerbox.