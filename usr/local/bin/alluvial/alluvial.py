#!/usr/bin/env python
# copyright klaatu @member dot fsf dot org
#
# GPLv3
# This program is free software: you can redistribute it 
# and/or modify it under the terms of the 
# GNU General Public License as published by the 
# Free Software Foundation, either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied 
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public 
# License along with this program.  If not, see 
# http://www.gnu.org/licenses/

import pygame
from pygame.locals import *
from evdev import InputDevice, categorize, ecodes
from select import select
dev = InputDevice('/dev/input/event0')
print("debug", dev)

pygame.mixer.init()

SOUNDS = '/usr/local/bin/alluvial/'
UP = pygame.mixer.Sound(SOUNDS + 'cmaj.ogg')      # Guitar
RIGHT = pygame.mixer.Sound(SOUNDS + 'gmaj.ogg')   # Guitar
DOWN = pygame.mixer.Sound(SOUNDS + 'amaj.ogg')    # Guitar
LEFT = pygame.mixer.Sound(SOUNDS + 'bmaj.ogg')    # Guitar

SPACE = pygame.mixer.Sound(SOUNDS + 'b.ogg')      # Drum

LMB = pygame.mixer.Sound(SOUNDS + 'drum.ogg')     # Pno
#RMB = pygame.mixer.Sound(SOUNDS + 'RMB.ogg')

W = pygame.mixer.Sound(SOUNDS + 'g.ogg')          # Pno
A = pygame.mixer.Sound(SOUNDS + 'e.ogg')          # Pno 
S = pygame.mixer.Sound(SOUNDS + 'a.ogg')          # Pno 
D = pygame.mixer.Sound(SOUNDS + 'f.ogg')          # Pno 
F = pygame.mixer.Sound(SOUNDS + 'c.ogg')          # Pno
G = pygame.mixer.Sound(SOUNDS + 'd.ogg')          # Pno

main = True
while main == True:
    r,w,x = select([dev], [], [])
    for event in dev.read():

        #if event.type == ecodes.KEY[272]:
        if event.type == ecodes.EV_KEY:
            APRESS = categorize(event)
            print("debug - you pressed something", APRESS)
            ADICT = str(APRESS).split(" ")
            print('debug')
            print(ADICT)[-1]
            print(ADICT)[4]

            # key events married to sounds here
            if ADICT[-1] == 'down':
                print(ADICT)[-1]
                if ADICT[4] == '103':
                    print(ADICT)[4]
                    UP.play(loops = 0)
                if ADICT[4] == '105':
                    print(ADICT)[4]
                    RIGHT.play(loops = 0)
                if ADICT[4] == '108':
                    print(ADICT)[4]
                    DOWN.play(loops = 0)
                if ADICT[4] == '106':
                    print(ADICT)[4]
                    LEFT.play(loops = 0)

                if ADICT[4] == '57':
                    print(ADICT)[4]
                    SPACE.play(loops = 0)

                if ADICT[4] == '272':
                    print(ADICT)[4]
                    LMB.play(loops = 0)
                if ADICT[4] == '273':
                    print(ADICT)[4]
                    LMB.play(loops = 0)

                if ADICT[4] == '17':
                    print(ADICT)[4]
                    W.play(loops = 0)
                if ADICT[4] == '30':
                    print(ADICT)[4]
                    A.play(loops = 0)
                if ADICT[4] == '31':
                    print(ADICT)[4]
                    S.play(loops = 0)
                if ADICT[4] == '32':
                    print(ADICT)[4]
                    D.play(loops = 0)
                if ADICT[4] == '33':
                    print(ADICT)[4]
                    F.play(loops = 0)
                if ADICT[4] == '34':
                    print(ADICT)[4]
                    G.play(loops = 0)
